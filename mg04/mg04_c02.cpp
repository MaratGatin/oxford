//
// Created by Marat Gatin on 10/08/2017.
//

#include "stdio.h"

int main(){
    int n, count = 0, c;
    scanf("%d", &n);
    while(n--){
        scanf("%d", &c);
        if (!c)
            count++;
    }
    printf("%d", count);
    return 0;
}
