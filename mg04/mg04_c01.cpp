//
// Created by Marat Gatin on 10/08/2017.
//

#include "stdio.h"

int main(){
    int n, sum = 0;
    scanf("%d", &n);
    while (n)
        sum += n-- * n;
    printf("%d", sum);
    return 0;
}