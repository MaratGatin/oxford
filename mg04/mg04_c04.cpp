//
// Created by Marat Gatin on 10/08/2017.
//

#include "stdio.h"

int main(){
    int n, max, num;
    scanf("%d", &n);

    scanf("%d", &max);//прочитаем первый элемент
    n--;

    while (n--){//обрабатываем остальные
        scanf("%d", &num);
        if (num > max)
            max = num;
    }
    printf("%d", max);
    return 0;
}