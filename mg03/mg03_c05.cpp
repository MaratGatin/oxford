//
// Created by Marat Gatin on 10/08/2017.
//

#include "stdio.h"

int main(){
    int n, sum = 0, c;
    scanf("%d", &n);
    while(n--){
        scanf("%d", &c);
        sum += c;
    }
    printf("%d", sum);
    return 0;
}