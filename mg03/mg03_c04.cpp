//
// Created by Marat Gatin on 10/08/2017.
//

#include "stdio.h"
#include <cmath>

int main(){
    int n;
    scanf("%d", &n);
    for(int i = 1; i <= n; i++)
        if (n % i == 0)
            printf("%d ", i);
    return 0;
}