//
// Created by Marat Gatin on 10/08/2017.
//

#include "stdio.h"

int main(){
    int n;
    scanf("%d", &n);
    printf("%s", n % 4 == 0 && n % 100 || n % 400 == 0 ? "YES" : "NO");
    return 0;
}