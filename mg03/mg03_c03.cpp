//
// Created by Marat Gatin on 10/08/2017.
//

#include "stdio.h"

int main(){
    int n, fact = 1;
    scanf("%d", &n);
    while(n)
        fact *= n--;
    printf("%d\n", fact);
    return 0;
}