#include <stdio.h>

int main() {
    int n;
    scanf("%d", &n);
    n %= 24 * 60; //деление по модулю на кол-во минут в сутках
    printf("%d %d", n / 60, n % 60);
    return 0;
}