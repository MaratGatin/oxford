//
// Created by Marat Gatin on 09/08/2017.
//

#include "stdio.h"

int main(){
    int n, sum_minutes;
    scanf("%d", &n);
    sum_minutes = 55 * n - (n % 2 == 1 ? 10 : 15);
    printf("%d %d", 9 + sum_minutes / 60, sum_minutes % 60);
    return 0;
}