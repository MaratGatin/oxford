//
// Created by Marat Gatin on 09/08/2017.
//
#include "stdio.h"

int main(){
    int n, m;
    scanf("%d%d", &n, &m);
    printf("%d", m / n + (m % n != 0));
    return 0;
}
