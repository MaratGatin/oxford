//
// Created by Marat Gatin on 09/08/2017.
//

#include "stdio.h"

int main(){
    int n, sum = 0;
    scanf("%d", &n);
    while (n){
        sum += n % 10;
        n /= 10;
    }
    printf("%d", sum);
    return 0;
}